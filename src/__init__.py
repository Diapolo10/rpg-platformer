#!python3

"""
This is a practice project designed specifically to
teach its author to use the Arcade module for Python.
It may or may not produce anything tangible.
If an actual product is created, it will be put on Steam.

I, {author}, hereby declare that whatever this project
achieves, it shall not go to waste.

Credits: {credits}
"""

__author__     = "Lari Liuhamo"
__copyright__  = f"Copyright 2017, {__author__}."
__credits__    = [
    __author__,
    "Paul V Craven, the creator of arcade",
    "Guido van Rossum, the creator of Python",
    "and many, many others."
]

__license__    = "GNU General Public License v3.0"
__version__    = "0.1.0"
__maintainer__ = __author__
__email__      = "lari.liuhamo+rpgenie@gmail.com"
__status__     = "alpha"

__doc__ = __doc__.format(
    author = __author__,
    credits = ", ".join(__credits__)
        if len(__credits__) > 1
        else __credits__[0]
            if __credits__
            else "None"
)
