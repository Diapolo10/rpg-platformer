#!python3

# Built-ins
import csv
import math
import sys
import threading
from pathlib import Path
from typing import List, Optional, Tuple, Union
from queue import Queue

# Third-party
import arcade

# First-party
# from rpgenie import *
from .config import *

if getattr(sys, 'frozen', False):
    # Running as a PyInstaller EXE
    __file__ = sys._MEIPASS #pylint: disable=E1101

def get_map(filename: Union[str, Path]) -> Tuple[Tuple[int]]:
    """
    Returns a map as an integer matrix

    filename: the name of a CSV file. Expected to be located inside ./assets/maps
    """

    filepath = Path(__file__).parent / 'assets' / 'maps' / filename
    with open(filepath, newline='') as csvfile:
        
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect=dialect)

        return tuple(
            tuple(map(int, row))
            for row in reader
        )
        

class RGB:

    def __init__(self, red: int, green: int, blue: int, alpha: Optional[int] = None, *args, **kwargs):

        self.red   = red % 256
        self.green = green % 256
        self.blue  = blue % 256
        if alpha is not None:
            self.alpha = alpha % 256

    @property
    def values(self) -> Tuple[int]:
        if getattr(self, 'alpha', None) is not None:
            return (self.red, self.green, self.blue, self.alpha)
        return (self.red, self.green, self.blue)

    @values.getter
    def _values_get(self, values: Tuple[int]):

        self.red   = values[0] % 256
        self.green = values[1] % 256
        self.blue  = values[2] % 256
        
        if getattr(self, 'alpha', None) is not None:
            try:
                self.alpha = values[3] % 256
            except IndexError:
                raise ValueError(f"not enough values to unpack (expected 4, got {len(values)})")

    def __repr__(self):
        if getattr(self, 'alpha', None) is not None:
            return f"RGBA({self.red}, {self.green}, {self.blue}, {self.alpha})"
        return f"RGB({self.red}, {self.green}, {self.blue})"


class HSL:

    def __init__(self, hue: int, saturation: int, lightness: int, alpha: Optional[int] = None, *args, **kwargs):

        self.hue        = hue % 256
        self.saturation = saturation % 256
        self.lightness  = lightness % 256
        if alpha is not None:
            self.alpha = alpha % 256

    @property
    def values(self) -> Tuple[int]:
        if getattr(self, 'alpha', None) is not None:
            return (self.hue, self.saturation, self.lightness, self.alpha)
        return (self.hue, self.saturation, self.lightness)

    @values.getter
    def _values_get(self, values: Tuple[int]):

        self.hue        = values[0] % 256
        self.saturation = values[1] % 256
        self.lightness  = values[2] % 256
        
        if getattr(self, 'alpha', None) is not None:
            try:
                self.alpha = values[3] % 256
            except IndexError:
                raise ValueError(f"not enough values to unpack (expected 4, got {len(values)})")

    def __repr__(self):
        if getattr(self, 'alpha', None) is not None:
            return f"HSLA({self.hue}, {self.saturation}, {self.lightness}, {self.alpha})"
        return f"HSL({self.hue}, {self.saturation}, {self.lightness})"

class Application(arcade.Window):
    """ Main application class """

    def __init__(self, width, height):
        super().__init__(width, height, title="BeatBoxBoy", resizable=True)

        arcade.set_background_color(RGB(192, 255, 238).values)

        ##### Platformer sprite lists #####
        self.background_list_far = None
        self.background_list = None
        
        self.moving_platform_list = None
        self.static_platform_list = None

        self.player_list = None
        self.npc_list = None

        self.foreground_list = None

        self.hud_list = None
        ###################################

        ######## RPG sprite lists #########
        self.map_tile_list = None
        self.map_wall_list = None
        self.map_object_list = None

        self.map_player_list = None
        self.map_npc_list = None

        self.map_hud_list = None
        ###################################

        # Player setup
        self.player_sprite = None
        self.player_map_sprite = None

        # Physics engine
        self.physics_engine = None

        # Used for scrolling map
        self.view_left = 0
        self.view_bottom = 0

        self.game_over = False

    def setup(self):
        pass
