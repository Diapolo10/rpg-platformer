#!python3

# Constants for the game

SPRITE_SCALING = 0.5

SCREEN_WIDTH  = 640
SCREEN_HEIGHT = 480

# How many pixels to keep as a minimum margin between the character
# and the edge of the screen.
VIEWPORT_MARGIN = 40
RIGHT_MARGIN    = 150

# Physics (platformer sections)
MOVEMENT_SPEED = 5
JUMP_SPEED     = 14
GRAVITY        = 0.5